#ifndef __BUTTON_H
#define __BUTTON_H

#include "stm32f1xx_hal.h"
#include <stdbool.h>

#define BUTTON_CTR_SENSOR_1 0
#define BUTTON_CTR_SENSOR_2 1

typedef enum
{
	BUTTON_STATE_SELECT = 0x00, 
  BUTTON_STATE_NOT_SELECT = 0x01,
	BUTTON_STATE_SET_ALARM = 0x02,
}BUTTON_STATE;


typedef struct {
	uint32_t time_double_click ;  // thoi gian giua 2 lan an
	uint32_t time_press ;  //  thoi gian tai thoi diem an
	BUTTON_STATE State;
	float temp_alarm; 
}BUTTON_CTRL;
void Check_SenSor_Run ();
void Button_Process ();
#endif