#include "LM35.h"
#include "lcd_16x2.h"
#include <stdio.h>
#include "stm32f1xx_hal_adc.h"
#include "BUTTON.h"

extern BUTTON_CTRL Button[2] ;
float Start_get_temp_Sensor (ADC_HandleTypeDef *hander_adc, int BTN_ID)
{	
	uint16_t value;
	float temp_get ;
	char * temp_string;
	HAL_ADC_Start(hander_adc);
	HAL_ADC_PollForConversion(hander_adc, 1000);
	value =HAL_ADC_GetValue(hander_adc);
	temp_get = value * 0.08;
	HAL_ADC_Stop (hander_adc);
	if(Button[BTN_ID].temp_alarm < temp_get)
		{
			sprintf(temp_string ,"WANNIG");
			Lcd_clear_display();
			Lcd_write_string(temp_string);
		}
	else
		{
			sprintf(temp_string ,"SENSOR%d:%f",BTN_ID+1, temp_get);
			Lcd_clear_display();
			Lcd_write_string(temp_string);
		}
	return temp_get;
}