#include "BUTTON.h"
#include "lcd_16x2.h"
#include "LM35.h"

extern uint32_t g_symtime ;
extern uint8_t sub_count_temp;
extern uint8_t add_count_temp;
extern int g_value_alarm ;
extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc2;

BUTTON_CTRL Button[2] ;
// ************************** Low Level Layer *****************************************************//
static void Temp_set_alarm (BUTTON_CTRL * BTN)
{
		g_value_alarm = g_value_alarm + add_count_temp - sub_count_temp ;
		BTN->temp_alarm = g_value_alarm ;
		Lcd_clear_display();
		Lcd_write_int((int)g_value_alarm);
	// lat them phan hien thi lcd
}
static void Select_sensor();
void Check_SenSor_Run ()
{	
	if(Button[BUTTON_CTR_SENSOR_1].State != BUTTON_STATE_SET_ALARM && Button[BUTTON_CTR_SENSOR_2].State != BUTTON_STATE_SET_ALARM)
		{
			if(Button[BUTTON_CTR_SENSOR_1].time_press < Button[BUTTON_CTR_SENSOR_2].time_press)
			{
				Button[BUTTON_CTR_SENSOR_1].State = BUTTON_STATE_NOT_SELECT ;
				Button[BUTTON_CTR_SENSOR_2].State = BUTTON_STATE_SELECT ;
			}
			else
			{
				Button[BUTTON_CTR_SENSOR_2].State = BUTTON_STATE_NOT_SELECT ;
				Button[BUTTON_CTR_SENSOR_1].State = BUTTON_STATE_SELECT ;
			}
		}

}
void Button_Process ()
{		
	for(uint8_t button_id =0; button_id <2 ; button_id ++ )
	{
		if(Button[button_id].State != BUTTON_STATE_SET_ALARM)
		{
			if((uint32_t)( Button[button_id].time_double_click) > 100 &&(uint32_t)(Button[button_id].time_double_click) <1000)
			{
				// HAM SET NGUONG
				Button[button_id].State = BUTTON_STATE_SET_ALARM ;  // thi nut dc chon
				Temp_set_alarm(Button+button_id);	
				break;
			}
			else
			{	// neu khong set nguong thi lay gia tri do
				if(Button[button_id].State == BUTTON_STATE_SELECT )
				{
					if(button_id ==0 ) 
					{
						Start_get_temp_Sensor(&hadc1, button_id);
					}
					else Start_get_temp_Sensor(&hadc2, button_id);
				}
				
			}
		}
		else
		{
			Temp_set_alarm(Button+button_id);	
			break;
		}
	}
		
}
